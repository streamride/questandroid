package io.quests;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.quests.db.CacheManager;
import io.quests.db.DBManager;
import io.quests.utils.PreferencesManager;
import io.quests.utils.ToastHandler;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by andreyzakharov on 28.05.16.
 */
@Module
public class AppModule {

    QuestApp mApplication;

    public AppModule(QuestApp application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    QuestApp providesApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
        // Application reference must come from AppModule.class
    PreferencesManager providesSharedPreferences(QuestApp application) {
        return new PreferencesManager(application);
    }

    @Provides
    @Singleton
    Cache provideOkHttpCache(QuestApp application) {
        int cacheSize = 10 * 1024 * 1024; // 10 MiB
        Cache cache = new Cache(application.getCacheDir(), cacheSize);
        return cache;
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient(Cache cache) {
        OkHttpClient client = new OkHttpClient.Builder().cache(cache).build();
        return client;
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(Constants.BASE_URL)
                .client(okHttpClient)
                .build();
        return retrofit;
    }

    @Singleton
    @Provides
    ToastHandler provideToastHandler() {
        return new ToastHandler(mApplication.getApplicationContext());
    }

    @Singleton
    @Provides
    DBManager providesDBManager() {
        return new DBManager(mApplication);
    }

    @Singleton
    @Provides
    CacheManager provideCacheManager(DBManager pDBManager) {
        return new CacheManager(pDBManager);
    }
}
