package io.quests.network;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.quests.QuestApp;

/**
 * Created by andreyzakharov on 11.06.16.
 */
@Module
public class NetworkModule {

    @Singleton
    @Provides
    public RestClient provideRedditClient(QuestApp app){
        return new RestClient(app);
    }
}
