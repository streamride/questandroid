package io.quests.network;

import io.quests.Constants;
import io.quests.QuestApp;
import io.quests.data.managers.QuestContainer;
import io.quests.data.managers.QuestStepContainer;
import io.quests.data.models.Quest;
import io.quests.data.models.QuestStep;
import io.quests.data.models.User;
import rx.Observable;

/**
 * Created by andreyzakharov on 28.05.16.
 */

public class RestClient extends RestService implements Constants {


    private final QuestProvider provider;

    public RestClient(QuestApp pQuestApp) {
        super(pQuestApp);
        provider = buildRetrofit(BASE_URL).create(QuestProvider.class);
    }


    public Observable<User> authenticateUser(String accessToken) {
        return provider.getUser(accessToken);
    }

    public Observable<QuestContainer> getQuests() {
        return provider.getQuests();
    }

    public Observable<Quest> createQuest(String name, int type) {
        return provider.createQuest(type, name);
    }

    public Observable<QuestStepContainer> getSteps(Quest pQuest) {
        return provider.getSteps(pQuest.id);
    }

    public Observable<QuestStep> createStep(String questId, String questType, String name, double lat, double lng, int radius, String text) {
        return provider.createStep(questId,
                                   questType,
                                   name,
                                   lat,
                                   lng,
                                   radius,
                                   text
                                  );
    }

    public Observable<QuestStep> updateStep(String markId, String questId, String questType, String name, double lat, double lng, int radius, String text) {
        return provider.updateStep(markId,
                                   questId,
                                   questType,
                                   name,
                                   lat,
                                   lng,
                                   radius,
                                   text
                                  );
    }

    public Observable<Quest> updateQuest(String name, String about, int type) {
        return provider.updateQuest(name, about, type);
    }

    public Observable<QuestContainer> getMyQuests() {
        return provider.getMyQuests();
    }
}
