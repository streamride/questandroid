package io.quests.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import io.quests.QuestApp;
import okhttp3.Authenticator;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by andreyzakharov on 11.06.16.
 */

public class RestService {

    protected final QuestApp mApp;
    private final Retrofit.Builder retrofitBuilder;

    public RestService(QuestApp pQuestApp) {
        mApp = pQuestApp;
        retrofitBuilder = getRetrofitBuilder();
    }

    public Retrofit buildRetrofit(String host) {
        return retrofitBuilder.baseUrl(host).build();
    }

    public boolean isOnline() {
        return mApp.getConnectionManager().isOnline();
    }

    private Retrofit.Builder getRetrofitBuilder() {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(myHttpClient());
    }

    private OkHttpClient myHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        builder.addInterceptor(chain -> {
            Request originalRequest = chain.request();
            Request.Builder requestBuilder = originalRequest.newBuilder();
            String credentials = getCredentials();
            requestBuilder.addHeader("Authorization", credentials);
            Request request = requestBuilder.build();

            Response.Builder responseBuilder = chain.proceed(request).newBuilder();
            return responseBuilder.build();
        });
        builder.authenticator(new MyAuthenticator());
        return builder.build();
    }


    private static Gson getGson() {
        return new GsonBuilder()
//                .registerTypeAdapter(BooleanDate.class, new BooleanDateDeserializer())
//                .registerTypeAdapter(RedditObject.class, new RedditObjectDeserializer())
//                .registerTypeAdapter(DateTime.class, new DateTimeDeserializer())
.create();
    }


    private String getCredentials() {
        return "Token " + mApp.getSharedPreferences().getAuthToken();
    }


    private class MyAuthenticator implements Authenticator {

        @Override
        public Request authenticate(Route route, Response response) throws IOException {
            if ((response.code() == 401 || response.code() == 403) && response.message() != null) { //unauthorized
                mApp.startAuthActivity();
            }
            return null;
        }
    }


}
