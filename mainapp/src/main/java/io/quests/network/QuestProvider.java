package io.quests.network;

import io.quests.data.managers.QuestContainer;
import io.quests.data.managers.QuestStepContainer;
import io.quests.data.models.Quest;
import io.quests.data.models.QuestStep;
import io.quests.data.models.User;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by andreyzakharov on 12.06.16.
 */

public interface QuestProvider {

    String NAMESPACE = "api/v1/";
    String ACCESS_TOKEN = "access_token";

    @GET("users/register-by-token/facebook/")
    Observable<User> getUser(@Query("access_token") String accessToken);

    @GET(NAMESPACE + "quests/")
    Observable<QuestContainer> getQuests();

    @FormUrlEncoded
    @POST(NAMESPACE + "quests/")
    Observable<Quest> createQuest(@Field("type") int type, @Field("name") String name);

    @GET(NAMESPACE + "marks/{questId}/sequence/")
    Observable<QuestStepContainer> getSteps(@Path("questId") String pId);

    @FormUrlEncoded
    @POST(NAMESPACE + "marks/")
    Observable<QuestStep> createStep(@Field("quest") String questId,
                                     @Field("quest_type") String questType,
                                     @Field("name") String name,
                                     @Field("lat") double lat,
                                     @Field("lng") double lng,
                                     @Field("radius") int radius,
                                     @Field("text") String text);

    @FormUrlEncoded
    @PUT(NAMESPACE + "marks/")
    Observable<QuestStep> updateStep(@Field("id") String markId,
                                     @Field("quest") String questId,
                                     @Field("quest_type") String questType,
                                     @Field("name") String name,
                                     @Field("lat") double lat,
                                     @Field("lng") double lng,
                                     @Field("radius") int radius,
                                     @Field("text") String text);

    @FormUrlEncoded
    @PUT(NAMESPACE + "quests/")
    Observable<Quest> updateQuest(String pName, String pAbout, int pType);

    @GET(NAMESPACE + "quests/?my=1")
    Observable<QuestContainer> getMyQuests();


}
