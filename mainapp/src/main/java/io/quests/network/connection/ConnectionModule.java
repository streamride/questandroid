package io.quests.network.connection;


import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.quests.QuestApp;
import io.quests.utils.ToastHandler;

@Module
public class ConnectionModule {
    @Singleton
    @Provides
    public ConnectionManager provideConnectionStatus(QuestApp app, ToastHandler handler){
        return new ConnectionManager(app.getApplicationContext(), handler);
    }
}
