package io.quests;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.vk.sdk.VKSdk;

import javax.inject.Inject;

import io.quests.db.CacheManager;
import io.quests.db.DBManager;
import io.quests.network.RestClient;
import io.quests.network.connection.ConnectionManager;
import io.quests.ui.StartActivity;
import io.quests.utils.PreferencesManager;
import io.quests.utils.ToastHandler;
import retrofit2.Retrofit;

/**
 * Created by andreyzakharov on 15.05.16.
 */
public class QuestApp extends Application {

    private AppComponent component;

    @Inject
    PreferencesManager mSharedPreferences;

    @Inject
    ToastHandler toastHandler;

    @Inject
    Retrofit mRetrofit;

    @Inject
    DBManager mDBManager;

    @Inject
    CacheManager mCacheManager;

    @Inject
    RestClient mRestClient;

    @Inject
    ConnectionManager connectionManager;

    public static volatile Context applicationContext;


    @Override
    public void onCreate() {
        super.onCreate();
        VKSdk.initialize(getApplicationContext());

        component = DaggerAppComponent.builder()
                                      .appModule(new AppModule(this))
                                      .build();
        component.inject(this);
        applicationContext = getApplicationContext();
    }

    public PreferencesManager getSharedPreferences() {
        return mSharedPreferences;
    }

    public ToastHandler getToastHandler() {
        return toastHandler;
    }

    public Retrofit getRetrofit() {
        return mRetrofit;
    }

    public DBManager getDBManager() {
        return mDBManager;
    }

    public CacheManager getCacheManager() {
        return mCacheManager;
    }

    public RestClient getRestClient() {
        return mRestClient;
    }

    public ConnectionManager getConnectionManager() {
        return connectionManager;
    }

    public static QuestApp get(Context context){
        return (QuestApp) context.getApplicationContext();
    }

    public AppComponent component() {
        return component;
    }


    public void startAuthActivity() {
        Intent intent = new Intent(getApplicationContext(), StartActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        startActivity(intent);
    }

    public boolean isAuthorized() {
        return !TextUtils.isEmpty(getSharedPreferences().getAuthToken());
    }
}
