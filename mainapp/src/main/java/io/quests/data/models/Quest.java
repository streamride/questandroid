package io.quests.data.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by andreyzakharov on 15.05.16.
 */
public class Quest extends RealmObject {

    public static final int TYPE_MESSAGE = 0;
    public static final int TYPE_PUBLIC = 1;
    public static final int TYPE_PRIVATE = 2;


    @PrimaryKey
    public String id;

    private String name;

    private String type;

    private String picture;

    private String about;

    private User creator;

    private RealmList<User> participants;

    @SerializedName("online_only")
    private boolean onlineOnly;

    private boolean searchable;

    @SerializedName("is_draft")
    private boolean isDraft;


    public void setAbout(String pAbout) {
        about = pAbout;
    }

    public void setCreator(User pCreator) {
        creator = pCreator;
    }

    public void setName(String pName) {
        name = pName;
    }

    public void setOnlineOnly(boolean pOnlineOnly) {
        onlineOnly = pOnlineOnly;
    }

    public void setParticipants(RealmList<User> pParticipants) {
        participants = pParticipants;
    }

    public void setPicture(String pPicture) {
        picture = pPicture;
    }

    public void setSearchable(boolean pSearchable) {
        searchable = pSearchable;
    }

    public void setType(String pType) {
        type = pType;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getPicture() {
        return picture;
    }

    public String getAbout() {
        return about;
    }

    public User getCreator() {
        return creator;
    }

    public RealmList<User> getParticipants() {
        return participants;
    }

    public boolean isOnlineOnly() {
        return onlineOnly;
    }

    public boolean isSearchable() {
        return searchable;
    }

    public boolean isDraft() {
        return isDraft;
    }

    public void setDraft(boolean pDraft) {
        isDraft = pDraft;
    }
}
