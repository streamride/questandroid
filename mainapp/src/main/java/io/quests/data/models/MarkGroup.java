package io.quests.data.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by andreyzakharov on 02.06.16.
 */

public class MarkGroup extends RealmObject {
    @PrimaryKey
    public String id;

    private RealmList<User> participants;

    private String groupName;

    public RealmList<User> getParticipants() {
        return participants;
    }

    public void setParticipants(RealmList<User> pParticipants) {
        participants = pParticipants;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String pGroupName) {
        groupName = pGroupName;
    }
}
