package io.quests.data.models;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by andreyzakharov on 15.05.16.
 */
public class User extends RealmObject {

    @SerializedName("id")
    @PrimaryKey
    public String id;

    @SerializedName("username")
    private String userName;

    @SerializedName("avatar")
    private String avatar;

    @SerializedName("email")
    private String email;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("gender")
    private String gender;

    @SerializedName("country")
    private String country;

    @SerializedName("city")
    private String city;

    @SerializedName("token")
    private String accessToken;

    public void setAvatar(String pAvatar) {
        avatar = pAvatar;
    }

    public void setCity(String pCity) {
        city = pCity;
    }

    public void setCountry(String pCountry) {
        country = pCountry;
    }

    public void setEmail(String pEmail) {
        email = pEmail;
    }

    public void setFirstName(String pFirstName) {
        firstName = pFirstName;
    }

    public void setGender(String pGender) {
        gender = pGender;
    }

    public void setLastName(String pLastName) {
        lastName = pLastName;
    }

    public void setUserName(String pUserName) {
        userName = pUserName;
    }


    public String getUserName() {
        return userName;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGender() {
        return gender;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String pAccessToken) {
        accessToken = pAccessToken;
    }

    public boolean isCurrentUser() {
        return TextUtils.isEmpty(getAccessToken());
    }
}
