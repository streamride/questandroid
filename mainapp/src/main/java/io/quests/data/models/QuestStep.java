package io.quests.data.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by andreyzakharov on 18.06.16.
 */

public class QuestStep extends RealmObject {

    @PrimaryKey
    public int id;

    private Mark mark;

    private Quest quest;

    private int position = -1;


    public Mark getMark() {
        return mark;
    }

    public void setMark(Mark pMark) {
        mark = pMark;
    }

    public Quest getQuest() {
        return quest;
    }

    public void setQuest(Quest pQuest) {
        quest = pQuest;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int pPosition) {
        position = pPosition;
    }


    public boolean isNew() {
        return position == -1;
    }


}
