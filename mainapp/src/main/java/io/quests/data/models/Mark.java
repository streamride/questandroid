package io.quests.data.models;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by andreyzakharov on 02.06.16.
 */

public class Mark extends RealmObject {

    @PrimaryKey
    public String id;

    @SerializedName("text")
    private String text;
    @SerializedName("name")
    private String name;
    @SerializedName("quest")
    private String mQuest;
    @SerializedName("from_user")
    private User fromUser;
    @SerializedName("created_date")
    private long createdDate;
    @SerializedName("radius")
    private int radius;
    @SerializedName("lat")
    private double lat;
    @SerializedName("lng")
    private double lng;
    @SerializedName("read_only_in_point")
    private boolean readOnlyInPoint;
    @SerializedName("type_result")
    private String typeResult;
    @SerializedName("type_trigger")
    private String typeTrigger;

    private String mediaPicture;


    public String getName() {
        return name;
    }

    public void setName(String pName) {
        name = pName;
    }

    public String getQuest() {
        return mQuest;
    }

    public void setQuest(String pQuest) {
        mQuest = pQuest;
    }

    public User getFromUser() {
        return fromUser;
    }

    public void setFromUser(User pFromUser) {
        fromUser = pFromUser;
    }

    public long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(long pCreatedDate) {
        createdDate = pCreatedDate;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int pRadius) {
        radius = pRadius;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double pLat) {
        lat = pLat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double pLng) {
        lng = pLng;
    }

    public boolean isReadOnlyInPoint() {
        return readOnlyInPoint;
    }

    public void setReadOnlyInPoint(boolean pReadOnlyInPoint) {
        readOnlyInPoint = pReadOnlyInPoint;
    }

    public String getTypeResult() {
        return typeResult;
    }

    public void setTypeResult(String pTypeResult) {
        typeResult = pTypeResult;
    }

    public String getTypeTrigger() {
        return typeTrigger;
    }

    public void setTypeTrigger(String pTypeTrigger) {
        typeTrigger = pTypeTrigger;
    }

    public String getMediaPicture() {
        return mediaPicture;
    }

    public void setMediaPicture(String pMediaPicture) {
        mediaPicture = pMediaPicture;
    }

    public String getText() {
        return text;
    }

    public void setText(String pText) {
        text = pText;
    }

    public static Mark createDefaultMark(double pLat, double pLng) {
        Mark mark = new Mark();
        mark.setName(getDefaultName());
        mark.setText("");
        mark.setRadius(100);
        mark.setLat(pLat);
        mark.setLng(pLng);
        return mark;
    }

    private static String getDefaultName() {
        return "New point";
    }
}
