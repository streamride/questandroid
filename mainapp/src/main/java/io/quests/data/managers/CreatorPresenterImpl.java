package io.quests.data.managers;

import io.quests.QuestApp;
import io.quests.data.models.Quest;
import io.quests.network.RestClient;
import io.quests.ui.constructor.views.CreatorView;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by andreyzakharov on 16.06.16.
 */

public class CreatorPresenterImpl implements CreatorPresenter {

    private final QuestApp mQuestApp;
    private final CreatorView mCreatorView;

    public CreatorPresenterImpl(QuestApp pQuestApp, CreatorView pCreatorView) {
        mQuestApp = pQuestApp;
        mCreatorView = pCreatorView;
    }

    @Override
    public void createQuest(String name) {
        RestClient restClient = mQuestApp.getRestClient();
        if (restClient.isOnline()) {
            restClient.createQuest(name, Quest.TYPE_PUBLIC).subscribeOn(Schedulers.newThread())
                      .observeOn(AndroidSchedulers.mainThread())
                      .subscribe(new Observer<Quest>() {
                          @Override
                          public void onCompleted() {

                          }

                          @Override
                          public void onError(Throwable e) {

                          }

                          @Override
                          public void onNext(Quest pQuest) {
                              mQuestApp.getCacheManager().saveQuest(pQuest);
                              mCreatorView.created(pQuest);
                          }
                      });
        }
    }
}
