package io.quests.data.managers.game;

import io.quests.data.models.Quest;

/**
 * Created by andreyzakharov on 26.07.16.
 */

public interface GamePresenter {

    void loadSteps(Quest pQuest);
}
