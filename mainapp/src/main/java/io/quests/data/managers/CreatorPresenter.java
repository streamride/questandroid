package io.quests.data.managers;

/**
 * Created by andreyzakharov on 16.06.16.
 */

public interface CreatorPresenter {

    void createQuest(String name);

}
