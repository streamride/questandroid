package io.quests.data.managers;

import java.util.List;

import io.quests.QuestApp;
import io.quests.data.models.Quest;
import io.quests.data.models.QuestStep;
import io.quests.network.RestClient;
import io.quests.ui.constructor.views.EditQuestView;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by andreyzakharov on 24.07.16.
 */

public class EditQuestPresenterImpl implements EditQuestPresenter {

    private final QuestApp mQuestApp;
    private final EditQuestView mEditQuestView;

    public EditQuestPresenterImpl(EditQuestView pEditQuestView, QuestApp pQuestApp) {
        mEditQuestView = pEditQuestView;
        mQuestApp = pQuestApp;
    }


    @Override
    public void loadSteps(Quest pQuest) {
        List<QuestStep> steps = mQuestApp.getCacheManager().getSteps(pQuest);
        if (steps != null && steps.size() > 0) {
            mEditQuestView.getPointsSegment().setSteps(steps);
        }
        RestClient restClient = mQuestApp.getRestClient();
        if (restClient.isOnline()) {
            restClient.getSteps(pQuest).subscribeOn(Schedulers.newThread())
                      .observeOn(AndroidSchedulers.mainThread())
                      .subscribe(new Observer<QuestStepContainer>() {
                          @Override
                          public void onCompleted() {

                          }

                          @Override
                          public void onError(Throwable e) {

                          }

                          @Override
                          public void onNext(QuestStepContainer pQuestStepContainer) {
                              mEditQuestView.getPointsSegment().setSteps(pQuestStepContainer.getQuestList());
                          }
                      });
        }
    }

    @Override
    public void loadPeople(Quest pQuest) {

    }

    @Override
    public void saveChanges(Quest pQuest) {

    }
}
