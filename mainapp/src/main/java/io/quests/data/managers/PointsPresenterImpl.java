package io.quests.data.managers;

import java.util.List;

import io.quests.QuestApp;
import io.quests.data.models.Quest;
import io.quests.data.models.QuestStep;
import io.quests.network.RestClient;
import io.quests.ui.constructor.views.PointsView;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by andreyzakharov on 17.06.16.
 */

public class PointsPresenterImpl implements PointsPresenter {

    private final PointsView mPointsView;
    private final QuestApp mQuestApp;

    public PointsPresenterImpl(PointsView pPointsView, QuestApp pQuestApp) {
        mPointsView = pPointsView;
        mQuestApp = pQuestApp;
    }


    @Override
    public void loadPoints(Quest pQuest) {
        List<QuestStep> steps = mQuestApp.getCacheManager().getSteps(pQuest);
        mPointsView.setSteps(steps);
        RestClient restClient = mQuestApp.getRestClient();
        if (restClient.isOnline()) {
            restClient.getSteps(pQuest).subscribeOn(Schedulers.newThread())
                      .observeOn(AndroidSchedulers.mainThread())
                      .subscribe(new Observer<QuestStepContainer>() {
                          @Override
                          public void onCompleted() {

                          }

                          @Override
                          public void onError(Throwable e) {

                          }

                          @Override
                          public void onNext(QuestStepContainer pQuestStepContainer) {
                              mQuestApp.getCacheManager().saveSteps(pQuestStepContainer.getQuestList());
                              mPointsView.setSteps(pQuestStepContainer.getQuestList());
                          }
                      });
        }
    }

    @Override
    public void createStep(QuestStep pQuestStep) {
        RestClient restClient = mQuestApp.getRestClient();
        if (restClient.isOnline()) {
            restClient.createStep(pQuestStep.getQuest().id, "quest", pQuestStep.getMark().getName(), pQuestStep.getMark().getLat(), pQuestStep.getMark().getLng(), pQuestStep.getMark().getRadius(), pQuestStep.getMark().getText())
                      .subscribeOn(Schedulers.newThread())
                      .observeOn(AndroidSchedulers.mainThread())
                      .subscribe(new Observer<QuestStep>() {
                          @Override
                          public void onCompleted() {

                          }

                          @Override
                          public void onError(Throwable e) {

                          }

                          @Override
                          public void onNext(QuestStep pQuestStep) {
                              mQuestApp.getCacheManager().saveStep(pQuestStep);
                          }
                      });
        }
    }

    @Override
    public void updateStep(QuestStep pQuestStep) {
        RestClient restClient = mQuestApp.getRestClient();
        if (restClient.isOnline()) {
            restClient.updateStep(pQuestStep.getMark().id, pQuestStep.getQuest().id, "quest", pQuestStep.getMark().getName(), pQuestStep.getMark().getLat(), pQuestStep.getMark().getLng(), pQuestStep.getMark().getRadius(), pQuestStep.getMark().getText())
                      .subscribeOn(Schedulers.newThread())
                      .observeOn(AndroidSchedulers.mainThread())
                      .subscribe(new Observer<QuestStep>() {
                          @Override
                          public void onCompleted() {

                          }

                          @Override
                          public void onError(Throwable e) {

                          }

                          @Override
                          public void onNext(QuestStep pQuestStep) {
                              mQuestApp.getCacheManager().saveStep(pQuestStep);
                          }
                      });
        }
    }
}
