package io.quests.data.managers;

import io.quests.data.models.Quest;

/**
 * Created by andreyzakharov on 19.06.16.
 */

public interface EditQuestPresenter {

    void loadSteps(Quest pQuest);

    void loadPeople(Quest pQuest);

    void saveChanges(Quest pQuest);
}
