package io.quests.data.managers;

import io.quests.data.models.Quest;
import io.quests.data.models.QuestStep;

/**
 * Created by andreyzakharov on 17.06.16.
 */

public interface PointsPresenter {
    void loadPoints(Quest pQuest);

    void createStep(QuestStep pQuestStep);

    void updateStep(QuestStep pQuestStep);
}
