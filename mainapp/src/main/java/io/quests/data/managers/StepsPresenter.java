package io.quests.data.managers;

import io.quests.data.models.Quest;

/**
 * Created by andreyzakharov on 17.06.16.
 */

public interface StepsPresenter {

    void loadSteps(Quest pQuest);

    void changeStepsPosition();
}
