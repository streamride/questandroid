package io.quests.data.managers;

/**
 * Created by andreyzakharov on 11.06.16.
 */

public interface QuestPresenter {

    void getQuest(int limit, int skip);

    void getMyQuests(int limit, int skip);
}
