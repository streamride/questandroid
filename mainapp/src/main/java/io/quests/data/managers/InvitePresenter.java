package io.quests.data.managers;

import io.quests.data.models.Quest;
import io.quests.data.models.User;

/**
 * Created by andreyzakharov on 18.07.16.
 */

public interface InvitePresenter {

    void loadInvited(Quest pQuest);

    void searchUser(String pSearchQuery);

    void inviteUser(Quest pQuest, User pUser);

    void deleteInvitation(Quest pQuest, User pUser);

    void loadRecentlyInvitated();


}
