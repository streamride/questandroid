package io.quests.data.managers;

import java.util.List;

import io.quests.QuestApp;
import io.quests.data.models.Quest;
import io.quests.data.models.QuestStep;
import io.quests.network.RestClient;
import io.quests.ui.constructor.views.StepsView;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by andreyzakharov on 17.06.16.
 */

public class StepsPresenterImpl implements StepsPresenter {

    private final StepsView mStepsView;
    private final QuestApp mQuestApp;

    public StepsPresenterImpl(StepsView pStepsView, QuestApp pQuestApp) {
        mStepsView = pStepsView;
        mQuestApp = pQuestApp;
    }

    @Override
    public void loadSteps(Quest pQuest) {
        List<QuestStep> steps = mQuestApp.getCacheManager().getSteps(pQuest);
        if (steps != null && steps.size() > 0) {
            mStepsView.getStepsListView().replaceWith(steps);
        }
        RestClient restClient = mQuestApp.getRestClient();
        if (restClient.isOnline()) {
            restClient.getSteps(pQuest).subscribeOn(Schedulers.newThread())
                      .observeOn(AndroidSchedulers.mainThread())
                      .subscribe(new Observer<QuestStepContainer>() {
                          @Override
                          public void onCompleted() {

                          }

                          @Override
                          public void onError(Throwable e) {

                          }

                          @Override
                          public void onNext(QuestStepContainer pQuestStepContainer) {
                              mStepsView.getStepsListView().replaceWith(pQuestStepContainer.getQuestList());
                          }
                      });
        }
    }

    @Override
    public void changeStepsPosition() {

    }
}
