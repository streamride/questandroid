package io.quests.data.managers;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.quests.data.models.Quest;

/**
 * Created by andreyzakharov on 13.06.16.
 */

public class QuestContainer {

    @SerializedName("data")
    private List<Quest> mQuestList;

    int count;

    public List<Quest> getQuestList() {
        return mQuestList;
    }

    public void setQuestList(List<Quest> pQuestList) {
        mQuestList = pQuestList;
    }
}
