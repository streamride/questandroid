package io.quests.data.managers;

import android.util.Log;

import io.quests.QuestApp;
import io.quests.data.models.Quest;
import io.quests.network.RestClient;
import io.quests.ui.quests.views.QuestView;
import io.realm.RealmResults;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by andreyzakharov on 11.06.16.
 */

public class QuestPresenterImpl implements QuestPresenter {

    public static final String QUEST_PRESENTER = QuestPresenterImpl.class.getSimpleName();
    private final QuestView mQuestView;
    private final QuestApp mQuestApp;

    public QuestPresenterImpl(QuestView pQuestView, QuestApp pQuestApp) {
        mQuestView = pQuestView;
        mQuestApp = pQuestApp;
    }

    @Override
    public void getQuest(int limit, int skip) {
        RestClient restClient = mQuestApp.getRestClient();
        mQuestView.showLoading();
        RealmResults<Quest> quests = mQuestApp.getCacheManager().getActiveQuests();
        mQuestView.getQuestRecyclerView().replaceWith(quests);
        if (restClient.isOnline()) {
            restClient.getQuests().subscribeOn(Schedulers.newThread())
                      .observeOn(AndroidSchedulers.mainThread())
                      .subscribe(new Observer<QuestContainer>() {
                          @Override
                          public void onCompleted() {

                          }

                          @Override
                          public void onError(Throwable e) {
                              Log.e(QUEST_PRESENTER, e.getLocalizedMessage());
                          }

                          @Override
                          public void onNext(QuestContainer pQuestContainer) {
                              mQuestApp.getCacheManager().saveQuests(pQuestContainer.getQuestList());
                              mQuestView.getQuestRecyclerView().replaceWith(pQuestContainer.getQuestList());
                          }
                      });
        }
    }

    @Override
    public void getMyQuests(int limit, int skip) {
        RestClient restClient = mQuestApp.getRestClient();
        mQuestView.showLoading();
        RealmResults<Quest> quests = mQuestApp.getCacheManager().getMyQuests();
        if (quests != null)
            mQuestView.getQuestRecyclerView().replaceWith(quests);
        if (restClient.isOnline()) {
            restClient.getMyQuests().subscribeOn(Schedulers.newThread())
                      .observeOn(AndroidSchedulers.mainThread())
                      .subscribe(new Observer<QuestContainer>() {
                          @Override
                          public void onCompleted() {

                          }

                          @Override
                          public void onError(Throwable e) {
                              Log.e(QUEST_PRESENTER, e.getLocalizedMessage());
                          }

                          @Override
                          public void onNext(QuestContainer pQuestContainer) {
                              mQuestApp.getCacheManager().saveQuests(pQuestContainer.getQuestList());
                              mQuestView.getQuestRecyclerView().replaceWith(pQuestContainer.getQuestList());
                          }
                      });
        }
    }
}
