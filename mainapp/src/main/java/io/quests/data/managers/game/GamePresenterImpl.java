package io.quests.data.managers.game;

import java.util.List;

import io.quests.QuestApp;
import io.quests.data.managers.QuestStepContainer;
import io.quests.data.models.Quest;
import io.quests.data.models.QuestStep;
import io.quests.network.RestClient;
import io.quests.ui.game.views.GameView;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by andreyzakharov on 26.07.16.
 */

public class GamePresenterImpl implements GamePresenter {

    private final QuestApp mQuestApp;
    private final GameView mGameView;

    public GamePresenterImpl(GameView pGameView, QuestApp pQuestApp) {
        mGameView = pGameView;
        mQuestApp = pQuestApp;
    }

    @Override
    public void loadSteps(Quest pQuest) {
        List<QuestStep> steps = mQuestApp.getCacheManager().getSteps(pQuest);
        mGameView.setSteps(steps);
        RestClient restClient = mQuestApp.getRestClient();
        if (restClient.isOnline()) {
            restClient.getSteps(pQuest).subscribeOn(Schedulers.newThread())
                      .observeOn(AndroidSchedulers.mainThread())
                      .subscribe(new Observer<QuestStepContainer>() {
                          @Override
                          public void onCompleted() {

                          }

                          @Override
                          public void onError(Throwable e) {

                          }

                          @Override
                          public void onNext(QuestStepContainer pQuestStepContainer) {
                              mQuestApp.getCacheManager().saveSteps(pQuestStepContainer.getQuestList());
                              mGameView.setSteps(pQuestStepContainer.getQuestList());
                          }
                      });
        }
    }
}
