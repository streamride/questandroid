package io.quests.data.managers;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import io.quests.data.models.QuestStep;

/**
 * Created by andreyzakharov on 17.07.16.
 */

public class QuestStepContainer {

    @SerializedName("data")
    private List<QuestStep> mQuestStepList;

    int count;

    public List<QuestStep> getQuestList() {
        return mQuestStepList;
    }

    public void setQuestList(List<QuestStep> pQuestList) {
        mQuestStepList = pQuestList;
    }
}
