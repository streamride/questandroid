package io.quests.data.managers;

import io.quests.data.models.Quest;
import io.quests.data.models.User;

/**
 * Created by andreyzakharov on 18.07.16.
 */

public class InvitePresenterImpl implements InvitePresenter {
    @Override
    public void loadInvited(Quest pQuest) {

    }

    @Override
    public void searchUser(String pSearchQuery) {

    }

    @Override
    public void inviteUser(Quest pQuest, User pUser) {

    }

    @Override
    public void deleteInvitation(Quest pQuest, User pUser) {

    }

    @Override
    public void loadRecentlyInvitated() {

    }
}
