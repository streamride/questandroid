package io.quests;

import javax.inject.Singleton;

import dagger.Component;
import io.quests.network.NetworkModule;
import io.quests.network.RestClient;
import io.quests.network.connection.ConnectionManager;
import io.quests.network.connection.ConnectionModule;
import io.quests.utils.PreferencesManager;
import io.quests.utils.ToastHandler;

/**
 * Created by andreyzakharov on 01.06.16.
 */
@Singleton
@Component(
        modules = {AppModule.class, NetworkModule.class, ConnectionModule.class}
)
public interface AppComponent {
    void inject(QuestApp app);

    QuestApp getApp();

    ToastHandler getToastHandler();

    PreferencesManager getSharedPreferences();

    RestClient getRestClient();

    ConnectionManager getConnectionManager();

}
