package io.quests.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

/**
 * Created by andreyzakharov on 22.06.16.
 */

public class SquaredContainer extends RelativeLayout {
    public SquaredContainer(Context context) {
        super(context);
    }

    public SquaredContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SquaredContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
        setMeasuredDimension(widthMeasureSpec, widthMeasureSpec);
    }
}
