package io.quests.ui.constructor;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.quests.AppComponent;
import io.quests.QuestApp;
import io.quests.R;
import io.quests.data.managers.EditQuestPresenter;
import io.quests.data.models.Quest;
import io.quests.databinding.FragmentEditQuestBinding;
import io.quests.ui.base.BaseFragment;
import io.quests.ui.constructor.components.DaggerEditQuestComponent;
import io.quests.ui.constructor.components.EditQuestComponent;
import io.quests.ui.constructor.modules.EditQuestModule;
import io.quests.ui.constructor.views.EditQuestView;
import io.quests.ui.constructor.widgets.EditQuestPointsSegment;

/**
 * Created by andreyzakharov on 18.06.16.
 */

public class EditQuestFragment extends BaseFragment implements EditQuestView {


    private static final String PARAM_QUEST_ID = "param_quest_id";

    EditQuestComponent component;
    private Quest mQuest;
    private QuestActivity activity;
    FragmentEditQuestBinding binding;
    @Inject
    EditQuestPresenter mEditQuestPresenter;

    public static EditQuestFragment newInstance(String questId) {
        EditQuestFragment editQuestFragment = new EditQuestFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_QUEST_ID, questId);
        editQuestFragment.setArguments(bundle);
        return editQuestFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        String questId = getArguments().getString(PARAM_QUEST_ID);
        mQuest = QuestApp.get(getContext()).getCacheManager().getQuestById(questId);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.edit_quest, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                getActivity().finish();
                break;
            case R.id.action_edit_steps:
                StepsConstructorFragment stepsConstructorFragment = StepsConstructorFragment.newInstance(mQuest.id);
                activity.setFragment(stepsConstructorFragment, true);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_quest, container, false);
        binding.saveBtn.setOnClickListener(pView -> saveChanges());
        binding.pointsSegment.setOnClickListener(pView -> {
            PointsConstructorFragment pointsConstructorFragment = PointsConstructorFragment.newInstance(mQuest.id);
            activity.setFragment(pointsConstructorFragment, true);
        });

        initSpinner();


        return binding.getRoot();
    }

    private void initPoints() {
        mEditQuestPresenter.loadSteps(mQuest);
    }

    private void initSpinner() {
        List<String> categories = new ArrayList<>();
        categories.add("Public");
        categories.add("Private");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, categories);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        binding.questTypeSpinner.setAdapter(dataAdapter);
    }

    private void saveChanges() {

    }

    @Override
    protected void setupComponent() {
        AppComponent appComponent = (AppComponent) QuestApp.get(getActivity()).component();
        component = DaggerEditQuestComponent.builder()
                                            .appComponent(appComponent)
                                            .editQuestModule(new EditQuestModule(this))
                                            .build();
        component.inject(this);
    }

    @Override
    protected void setupToolbar() {

        Toolbar mToolBar = (Toolbar) binding.getRoot().findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(mToolBar);
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (QuestActivity) getActivity();
        binding.questNameEdt.setText(mQuest.getName());
        binding.questAboutEdt.setText(mQuest.getAbout());
        initPoints();
    }

    @Override
    public EditQuestPointsSegment getPointsSegment() {
        return binding.pointsSegment;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
