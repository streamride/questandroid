package io.quests.ui.constructor.components;

import dagger.Component;
import io.quests.AppComponent;
import io.quests.ui.ActivityScope;
import io.quests.ui.constructor.PointsConstructorFragment;
import io.quests.ui.constructor.StartConstructorFragment;
import io.quests.ui.constructor.StepsConstructorFragment;
import io.quests.ui.constructor.start.CreatorModule;
import io.quests.ui.constructor.modules.PointsModule;
import io.quests.ui.constructor.modules.StepsModule;

/**
 * Created by andreyzakharov on 17.06.16.
 */

@ActivityScope
@Component(dependencies = AppComponent.class,
        modules = {CreatorModule.class, PointsModule.class, StepsModule.class})
public interface ConstructorActivityComponent {
    void inject(StartConstructorFragment pStartConstructorFragment);

    void inject(StepsConstructorFragment pStepsConstructorFragment);

    void inject(PointsConstructorFragment pPointsConstructorFragment);
}
