package io.quests.ui.constructor.start;

import dagger.Module;
import dagger.Provides;
import io.quests.QuestApp;
import io.quests.data.managers.CreatorPresenter;
import io.quests.data.managers.CreatorPresenterImpl;
import io.quests.ui.constructor.views.CreatorView;

/**
 * Created by andreyzakharov on 16.06.16.
 */

@Module
public class CreatorModule {

    private CreatorView mCreatorView;

    public CreatorModule(CreatorView pCreatorView) {
        mCreatorView = pCreatorView;
    }

    @Provides
    public CreatorPresenter provideCreatorPresenter(QuestApp pQuestApp) {
        return new CreatorPresenterImpl(pQuestApp, mCreatorView);
    }
}
