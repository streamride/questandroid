package io.quests.ui.constructor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;

import io.quests.R;
import io.quests.ui.base.BaseActivity;
import io.quests.ui.quests.CurrentQuestFragment;

/**
 * Created by andreyzakharov on 17.06.16.
 */

public class QuestActivity extends BaseActivity {


    private static final String PARAM_QUEST_ID = "param_quest_id";
    private static final String PARAM_EDIT = "param_edit";

    public static void startActivity(Context pContext, String questID) {
        startActivity(pContext, questID, true);
    }

    public static void startActivity(Context pContext, String questID, boolean isEdit) {

        Intent intent = new Intent(pContext, QuestActivity.class);
        if (!TextUtils.isEmpty(questID)) {
            Bundle bundle = new Bundle();
            bundle.putString(PARAM_QUEST_ID, questID);
            bundle.putBoolean(PARAM_EDIT, isEdit);
            intent.putExtras(bundle);
        }
        pContext.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmentcontainer);
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(PARAM_QUEST_ID)) {
            if (extras.containsKey(PARAM_EDIT) && extras.getBoolean(PARAM_EDIT)) {
                setFragment(EditQuestFragment.newInstance(extras.getString(PARAM_QUEST_ID)), false);
            } else {
                setFragment(CurrentQuestFragment.newInstance(extras.getString(PARAM_QUEST_ID)), false);
            }
        } else {
            setFragment(new StartConstructorFragment(), false);
        }
    }


    public void setFragment(Fragment pFragment, boolean addToBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragment, pFragment);
        if (addToBackStack)
            transaction.addToBackStack(null);
        transaction.commitAllowingStateLoss();
    }

}
