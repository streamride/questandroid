package io.quests.ui.constructor.components;

import dagger.Component;
import io.quests.AppComponent;
import io.quests.ui.FragmentScope;
import io.quests.ui.constructor.StepsListFragment;
import io.quests.ui.constructor.modules.StepsModule;

/**
 * Created by andreyzakharov on 18.06.16.
 */
@FragmentScope
@Component(dependencies = AppComponent.class,
        modules = {StepsModule.class})
public interface ConstructorStepComponent {
    void inject(StepsListFragment pStepsConstructorFragment);
}
