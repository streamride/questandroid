package io.quests.ui.constructor;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.quests.QuestApp;
import io.quests.R;
import io.quests.data.models.Quest;
import io.quests.databinding.FragmentConstructorStepsBinding;
import io.quests.ui.base.BaseFragment;
import io.quests.ui.constructor.components.ConstructorStepComponent;

/**
 * Created by andreyzakharov on 17.06.16.
 */

@Deprecated
public class StepsConstructorFragment extends BaseFragment {

    private static final String PARAM_QUEST_ID = "param_quest_id";
    ConstructorStepComponent component;

    private Quest mQuest;
    FragmentConstructorStepsBinding binding;


    public static StepsConstructorFragment newInstance(String pQuestId) {
        StepsConstructorFragment stepsConstructorFragment = new StepsConstructorFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_QUEST_ID, pQuestId);
        stepsConstructorFragment.setArguments(bundle);

        return stepsConstructorFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String questId = getArguments().getString(PARAM_QUEST_ID);
        mQuest = QuestApp.get(getContext()).getCacheManager().getQuestById(questId);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_constructor_steps, container, false);
//        mStepsListView = (StepsListView) view.findViewById(R.id.steps_lv);
//        mFabNewStep = (FloatingActionButton) view.findViewById(R.id.fab_new_step);
//        mFabNewStep.setOnClickListener(pView -> {
//            PointsConstructorFragment pointsConstructorFragment = PointsConstructorFragment.newInstance(mQuest.id);
//            activity.setFragment(pointsConstructorFragment, true);
//        });
        return binding.getRoot();
    }

    @Override
    protected void setupComponent() {
//        AppComponent appComponent = (AppComponent) QuestApp.get(getActivity()).component();
//        component = DaggerConstructorStepComponent.builder()
//                                                  .appComponent(appComponent)
//                                                  .stepsModule(new StepsModule(this))
//                                                  .build();
//        component.inject(this);
    }

    @Override
    protected void setupToolbar() {
//        Toolbar mToolBar = binding.toolbar;
//        AppCompatActivity activity = (AppCompatActivity) getActivity();
//        activity.setSupportActionBar(mToolBar);
//        ActionBar actionBar = activity.getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.setDisplayHomeAsUpEnabled(true);
//            actionBar.setDisplayShowTitleEnabled(true);
//        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initTabs();
    }


    private void initTabs() {
//        StepPagerAdapter stepPagerAdapter = new StepPagerAdapter(getChildFragmentManager());
//        binding.stepsVp.setAdapter(stepPagerAdapter);
//        TabLayout tablayout = binding.tablayout;
//        TabLayout.Tab list = tablayout.newTab();
//        TabLayout.Tab map = tablayout.newTab();
//        list.setText("List");
//        map.setText("Map");
//        tablayout.addTab(list);
//        tablayout.addTab(map);
//        tablayout.setupWithViewPager(binding.stepsVp);
    }

    public class StepPagerAdapter extends FragmentStatePagerAdapter {

        public StepPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return StepsListFragment.newInstance(mQuest.id);
                case 1:
                    return PointsConstructorFragment.newInstance(mQuest.id);
            }
            return null;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0:
                    return "List";
                case 1:
                    return "Map";
            }
            return super.getPageTitle(position);
        }
    }
}
