package io.quests.ui.constructor.components;

import dagger.Component;
import io.quests.AppComponent;
import io.quests.ui.ActivityScope;
import io.quests.ui.constructor.PointsConstructorFragment;
import io.quests.ui.constructor.modules.PointsModule;

/**
 * Created by andreyzakharov on 18.06.16.
 */

@ActivityScope
@Component(dependencies = AppComponent.class,
        modules = {PointsModule.class})
public interface ConstructorPointsComponent {
    void inject(PointsConstructorFragment pPointsConstructorFragment);
}
