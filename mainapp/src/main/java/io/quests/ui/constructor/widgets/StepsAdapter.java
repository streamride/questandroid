package io.quests.ui.constructor.widgets;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.marshalchen.ultimaterecyclerview.UltimateViewAdapter;

import java.util.ArrayList;
import java.util.List;

import io.quests.R;
import io.quests.data.models.QuestStep;

/**
 * Created by andreyzakharov on 17.06.16.
 */

public class StepsAdapter extends UltimateViewAdapter<StepsAdapter.ViewHolder> {


    private final Context mContext;
    private final LayoutInflater mInflater;
    private List<QuestStep> mQuestStep = new ArrayList<>();

    public StepsAdapter(Context pContext) {
        mContext = pContext;
        mInflater = LayoutInflater.from(mContext);
    }

    public QuestStep getItem(int position) {
        return mQuestStep.get(position);
    }

    @Override
    public ViewHolder newFooterHolder(View view) {
        return null;
    }

    @Override
    public ViewHolder newHeaderHolder(View view) {
        return null;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        View view = mInflater.inflate(R.layout.item_constructor_step, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public int getAdapterItemCount() {
        return mQuestStep.size();
    }

    @Override
    public long generateHeaderId(int position) {
        return 0;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        QuestStep questStep = getItem(position);
        holder.mStepName.setText(questStep.getMark().getName());
    }


    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    public void replaceWith(List<QuestStep> pQuestSteps) {
        mQuestStep = pQuestSteps;
        notifyDataSetChanged();
    }

    static class ViewHolder extends UltimateRecyclerviewViewHolder {

        private final TextView mStepName;

        public ViewHolder(View itemView) {
            super(itemView);
            mStepName = (TextView) itemView.findViewById(R.id.step_name_tv);
        }
    }
}
