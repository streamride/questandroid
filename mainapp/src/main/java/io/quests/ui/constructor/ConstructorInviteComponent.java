package io.quests.ui.constructor;

import dagger.Component;
import io.quests.AppComponent;
import io.quests.ui.FragmentScope;
import io.quests.ui.constructor.modules.StepsModule;

/**
 * Created by andreyzakharov on 18.07.16.
 */

@FragmentScope
@Component(dependencies = AppComponent.class,
        modules = {StepsModule.class})
public interface ConstructorInviteComponent {

    void inject(InviteConstructorFragment pInviteConstructorFragment);
}
