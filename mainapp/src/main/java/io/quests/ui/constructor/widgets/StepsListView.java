package io.quests.ui.constructor.widgets;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.util.AttributeSet;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;

import java.util.List;

import io.quests.data.models.Mark;
import io.quests.data.models.QuestStep;

/**
 * Created by andreyzakharov on 17.06.16.
 */

public class StepsListView extends UltimateRecyclerView {

    private StepsAdapter stepsAdapter;

    public StepsListView(Context context) {
        super(context);
        init(context);
    }

    public StepsListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public StepsListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context pContext) {
        setLayoutManager(new LinearLayoutManager(pContext));
        stepsAdapter = new StepsAdapter(getContext());
        setAdapter(stepsAdapter);
    }

    public void replaceWith(List<QuestStep> pSteps) {
        stepsAdapter.replaceWith(pSteps);
    }
}
