package io.quests.ui.constructor.views;

import io.quests.data.models.Quest;

/**
 * Created by andreyzakharov on 16.06.16.
 */

public interface CreatorView {

    void createQuest(String title, String about);

    void created(Quest pQuest);
}
