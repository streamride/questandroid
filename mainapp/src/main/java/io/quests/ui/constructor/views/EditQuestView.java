package io.quests.ui.constructor.views;

import io.quests.ui.base.BaseView;
import io.quests.ui.constructor.widgets.EditQuestPointsSegment;

/**
 * Created by andreyzakharov on 24.07.16.
 */

public interface EditQuestView extends BaseView {


    EditQuestPointsSegment getPointsSegment();

}
