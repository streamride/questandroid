package io.quests.ui.constructor.components;

import dagger.Component;
import io.quests.AppComponent;
import io.quests.ui.ActivityScope;
import io.quests.ui.constructor.StartConstructorFragment;
import io.quests.ui.constructor.start.CreatorModule;

/**
 * Created by andreyzakharov on 18.06.16.
 */
@ActivityScope
@Component(dependencies = AppComponent.class,
        modules = {CreatorModule.class})
public interface ConstructorStartComponent {

    void inject(StartConstructorFragment pStartConstructorFragment);
}
