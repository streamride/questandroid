package io.quests.ui.constructor.modules;

import dagger.Module;
import dagger.Provides;
import io.quests.QuestApp;
import io.quests.data.managers.EditQuestPresenter;
import io.quests.data.managers.EditQuestPresenterImpl;
import io.quests.ui.constructor.views.EditQuestView;
import io.quests.ui.constructor.views.PointsView;

/**
 * Created by andreyzakharov on 18.06.16.
 */
@Module
public class EditQuestModule {

    private final EditQuestView mEditQuestView;

    public EditQuestModule(EditQuestView pPointsView) {
        mEditQuestView = pPointsView;
    }

    @Provides
    public EditQuestPresenter providesPointsPresenter(QuestApp pQuestApp) {
        return new EditQuestPresenterImpl(mEditQuestView, pQuestApp);
    }
}
