package io.quests.ui.constructor;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import com.google.android.gms.maps.model.LatLng;

import io.quests.R;
import io.quests.data.models.Mark;
import io.quests.data.models.QuestStep;
import io.quests.databinding.PointEditLayoutBinding;

/**
 * Created by andreyzakharov on 22.06.16.
 */

public class PointSettingsView extends LinearLayout {

    private PointEditLayoutBinding binding;
    private NameListener mNameListener;
    private QuestStep mQuestStep;

    public PointSettingsView(Context context) {
        super(context);
    }

    public PointSettingsView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public PointSettingsView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setNameListener(NameListener pNameListener) {
        mNameListener = pNameListener;
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.point_edit_layout, this, true);
        binding.markNameEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence pCharSequence, int pI, int pI1, int pI2) {

            }

            @Override
            public void onTextChanged(CharSequence pCharSequence, int pI, int pI1, int pI2) {
                if (mNameListener != null) {
                    mNameListener.onNamedChanged(getPointName(pCharSequence.toString()));
                }
                if (mQuestStep != null && mQuestStep.getMark() != null) {
                    mQuestStep.getMark().setName(pCharSequence.toString());
                }
                binding.markNameTitleTv.setText(getPointName(pCharSequence.toString()));
            }

            @Override
            public void afterTextChanged(Editable pEditable) {

            }
        });

        binding.markAboutEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence pCharSequence, int pI, int pI1, int pI2) {

            }

            @Override
            public void onTextChanged(CharSequence pCharSequence, int pI, int pI1, int pI2) {
                if (mQuestStep != null && mQuestStep.getMark() != null) {
                    mQuestStep.getMark().setText(pCharSequence.toString());
                }
            }

            @Override
            public void afterTextChanged(Editable pEditable) {

            }
        });
        binding.pointRadius.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar pSeekBar, int pI, boolean pB) {
                if (pB) {
                    if (mQuestStep != null && mQuestStep.getMark() != null) {
                        mQuestStep.getMark().setRadius(pSeekBar.getProgress());
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar pSeekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar pSeekBar) {

            }
        });
    }

    public QuestStep getQuestStep() {
        return mQuestStep;
    }

    public void setQuestStep(QuestStep pQuestStep, LatLng pLatLng) {
        mQuestStep = pQuestStep;
        setupMark(pLatLng);
    }

    private void setupMark(LatLng pLatLng) {
        Mark mark = mQuestStep.getMark();
        if (mark != null) {
            setFields(mark);
        } else {
            mark = Mark.createDefaultMark(pLatLng.latitude, pLatLng.longitude);
            mQuestStep.setMark(mark);
            setFields(mark);
        }
    }

    private void setFields(Mark pMark) {
        binding.markNameTitleTv.setText(pMark.getName());
        binding.markNameEdt.setText(pMark.getName());
        binding.markAboutEdt.setText(pMark.getText());
        binding.pointRadius.setProgress(pMark.getRadius());
    }

    private String getDefaultName() {
        return "New point";
    }

    private String getPointName(String name) {
        return !isMarkNameEmpty() ? name : getDefaultName();
    }

    public boolean isMarkNameEmpty() {
        return binding.markNameEdt.getText().length() == 0;
    }


    public interface NameListener {
        void onNamedChanged(String newName);
    }


}
