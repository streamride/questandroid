package io.quests.ui.constructor.widgets;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import java.util.List;

import io.quests.R;
import io.quests.data.models.QuestStep;
import io.quests.databinding.PointsSegmentBinding;

/**
 * Created by andreyzakharov on 24.07.16.
 */

public class EditQuestPointsSegment extends RelativeLayout {

    private PointsSegmentBinding binding;

    public EditQuestPointsSegment(Context context) {
        super(context);
    }

    public EditQuestPointsSegment(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditQuestPointsSegment(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        binding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.points_segment, this, true);
    }

    public void setSteps(List<QuestStep> pQuestSteps) {
        if (pQuestSteps != null && pQuestSteps.size() > 0) {
            int size = pQuestSteps.size();
            if (size > 4) {
                size = 4;
            }
            for (int i = 0; i < size; i++) {
                if (i == 0)
                    binding.firstStep.setText(pQuestSteps.get(i).getMark().getName());
                if (i == 1)
                    binding.secondStep.setText(pQuestSteps.get(i).getMark().getName());
                if (i == 2)
                    binding.thirdStep.setText(pQuestSteps.get(i).getMark().getName());
                if (i == 3)
                    binding.fourthStep.setText(pQuestSteps.get(i).getMark().getName());
            }


        }
    }
}
