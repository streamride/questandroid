package io.quests.ui.constructor.components;

import dagger.Component;
import io.quests.AppComponent;
import io.quests.ui.FragmentScope;
import io.quests.ui.constructor.StartConstructorFragment;
import io.quests.ui.constructor.start.CreatorModule;

/**
 * Created by andreyzakharov on 16.06.16.
 */
@FragmentScope
@Component(dependencies = AppComponent.class,
        modules = CreatorModule.class)
public interface CreatorComponent {
    void inject(StartConstructorFragment pStartConstructorFragment);
}
