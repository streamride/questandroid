package io.quests.ui.constructor;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import io.quests.AppComponent;
import io.quests.QuestApp;
import io.quests.R;
import io.quests.data.managers.StepsPresenter;
import io.quests.data.models.Quest;
import io.quests.databinding.FragmentStepsListBinding;
import io.quests.ui.base.BaseFragment;
import io.quests.ui.constructor.components.ConstructorStepComponent;
import io.quests.ui.constructor.components.DaggerConstructorStepComponent;
import io.quests.ui.constructor.modules.StepsModule;
import io.quests.ui.constructor.views.StepsView;
import io.quests.ui.constructor.widgets.StepsListView;

/**
 * Created by andreyzakharov on 24.07.16.
 */

public class StepsListFragment extends BaseFragment implements StepsView {

    private static final String PARAM_QUEST_ID = "param_quest_id";
    FragmentStepsListBinding binding;

    ConstructorStepComponent component;

    @Inject
    StepsPresenter mStepsPresenter;
    private Quest mQuest;

    public static StepsListFragment newInstance(String pQuestId) {
        StepsListFragment stepsConstructorFragment = new StepsListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_QUEST_ID, pQuestId);
        stepsConstructorFragment.setArguments(bundle);

        return stepsConstructorFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String questId = getArguments().getString(PARAM_QUEST_ID);
        mQuest = QuestApp.get(getContext()).getCacheManager().getQuestById(questId);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_steps_list, container, false);
        return binding.getRoot();
    }

    @Override
    protected void setupComponent() {
        AppComponent appComponent = (AppComponent) QuestApp.get(getActivity()).component();
        component = DaggerConstructorStepComponent.builder()
                                                  .appComponent(appComponent)
                                                  .stepsModule(new StepsModule(this))
                                                  .build();
        component.inject(this);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mStepsPresenter.loadSteps(mQuest);
    }

    @Override
    protected void setupToolbar() {

    }

    @Override
    public StepsListView getStepsListView() {
        return binding.stepsLv;
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }
}
