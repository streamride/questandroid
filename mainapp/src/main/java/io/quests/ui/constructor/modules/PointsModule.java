package io.quests.ui.constructor.modules;

import dagger.Module;
import dagger.Provides;
import io.quests.QuestApp;
import io.quests.data.managers.PointsPresenter;
import io.quests.data.managers.PointsPresenterImpl;
import io.quests.ui.constructor.views.PointsView;

/**
 * Created by andreyzakharov on 17.06.16.
 */

@Module
public class PointsModule {

    private final PointsView mPointsView;

    public PointsModule(PointsView pPointsView) {
        mPointsView = pPointsView;
    }

    @Provides
    public PointsPresenter providesPointsPresenter(QuestApp pQuestApp) {
        return new PointsPresenterImpl(mPointsView, pQuestApp);
    }
}
