package io.quests.ui.constructor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import javax.inject.Inject;

import io.quests.AppComponent;
import io.quests.QuestApp;
import io.quests.R;
import io.quests.data.managers.CreatorPresenter;
import io.quests.data.models.Quest;
import io.quests.ui.base.BaseFragment;
import io.quests.ui.constructor.components.ConstructorStartComponent;
import io.quests.ui.constructor.components.DaggerConstructorStartComponent;
import io.quests.ui.constructor.start.CreatorModule;
import io.quests.ui.constructor.views.CreatorView;

/**
 * Created by andreyzakharov on 16.06.16.
 */

public class StartConstructorFragment extends BaseFragment implements CreatorView {

    ConstructorStartComponent component;

    @Inject
    CreatorPresenter mCreatorPresenter;
    private EditText mQuestName;
    private EditText mQuestAbout;
    private View view;
    private QuestActivity activity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.constructor_start_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_next:
                createQuest(mQuestName.getText().toString().trim(), mQuestAbout.getText().toString());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void setupComponent() {
        AppComponent appComponent = (AppComponent) QuestApp.get(getActivity()).component();
        component = DaggerConstructorStartComponent.builder()
                                                   .appComponent(appComponent)
                                                   .creatorModule(new CreatorModule(this))
                                                   .build();
        component.inject(this);


    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_newquest_start, container, false);
        mQuestName = (EditText) view.findViewById(R.id.quest_name_edt);
        mQuestAbout = (EditText) view.findViewById(R.id.quest_about_edt);

        return view;
    }

    @Override
    protected void setupToolbar() {
        Toolbar mToolBar = (Toolbar) view.findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(mToolBar);
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (QuestActivity) getActivity();
    }


    @Override
    public void createQuest(String title, String about) {
        if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(about)) {
            mCreatorPresenter.createQuest(title);
        }
    }

    @Override
    public void created(Quest pQuest) {
        if (pQuest != null) {
            StepsConstructorFragment stepsConstructorFragment = StepsConstructorFragment.newInstance(pQuest.id);
            activity.setFragment(stepsConstructorFragment, true);
        }
    }
}
