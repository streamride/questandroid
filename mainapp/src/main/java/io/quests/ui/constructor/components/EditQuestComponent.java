package io.quests.ui.constructor.components;

import dagger.Component;
import io.quests.AppComponent;
import io.quests.ui.FragmentScope;
import io.quests.ui.constructor.EditQuestFragment;
import io.quests.ui.constructor.modules.EditQuestModule;

/**
 * Created by andreyzakharov on 18.06.16.
 */
@FragmentScope
@Component(dependencies = AppComponent.class,
        modules = EditQuestModule.class)
public interface EditQuestComponent {

    void inject(EditQuestFragment pEditQuestFragment);
}
