package io.quests.ui.constructor.views;

import io.quests.ui.base.BaseView;
import io.quests.ui.constructor.widgets.StepsListView;

/**
 * Created by andreyzakharov on 17.06.16.
 */

public interface StepsView extends BaseView {

    StepsListView getStepsListView();
}
