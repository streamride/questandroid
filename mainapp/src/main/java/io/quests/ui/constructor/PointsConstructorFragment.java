package io.quests.ui.constructor;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.quests.AppComponent;
import io.quests.QuestApp;
import io.quests.R;
import io.quests.data.managers.PointsPresenter;
import io.quests.data.models.Quest;
import io.quests.data.models.QuestStep;
import io.quests.databinding.FragmentConstructorPointsBinding;
import io.quests.ui.base.AskDialogFragment;
import io.quests.ui.base.BaseFragment;
import io.quests.ui.base.OkDialogFragment;
import io.quests.ui.constructor.behaviors.PointsBottomSheetBehavior;
import io.quests.ui.constructor.components.ConstructorPointsComponent;
import io.quests.ui.constructor.components.DaggerConstructorPointsComponent;
import io.quests.ui.constructor.modules.PointsModule;
import io.quests.ui.constructor.views.PointsView;
import io.quests.utils.AndroidUtils;

/**
 * Created by andreyzakharov on 17.06.16.
 */

public class PointsConstructorFragment extends BaseFragment implements PointsView, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String PARAM_QUEST_ID = "param_quest_id";
    ConstructorPointsComponent component;

    @Inject
    PointsPresenter mPointsPresenter;
    private SupportMapFragment mapFragment;
    private Quest mQuest;
    private GoogleMap mMap;
    private PointsBottomSheetBehavior<View> behavior;
    FragmentConstructorPointsBinding binding;
    private Map<LatLng, QuestStep> mQuestStepMap;
    private GoogleApiClient mGoogleApiClient;

    public static PointsConstructorFragment newInstance(String pQuestId) {
        PointsConstructorFragment pointsConstructorFragment = new PointsConstructorFragment();

        Bundle bundle = new Bundle();
        bundle.putString(PARAM_QUEST_ID, pQuestId);
        pointsConstructorFragment.setArguments(bundle);
        return pointsConstructorFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);
        String questId = getArguments().getString(PARAM_QUEST_ID);
        mQuest = QuestApp.get(getContext()).getCacheManager().getQuestById(questId);

    }

    @Override
    protected void setupComponent() {
        AppComponent appComponent = (AppComponent) QuestApp.get(getActivity()).component();
        component = DaggerConstructorPointsComponent.builder()
                                                    .appComponent(appComponent)
                                                    .pointsModule(new PointsModule(this))
                                                    .build();
        component.inject(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.quest_points_menu, menu);

    }

    @Override
    protected void setupToolbar() {
//        Toolbar mToolBar = binding.toolbar;
//        AppCompatActivity activity = (AppCompatActivity) getActivity();
//        activity.setSupportActionBar(mToolBar);
//        ActionBar actionBar = activity.getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.setDisplayHomeAsUpEnabled(true);
//            actionBar.setDisplayShowTitleEnabled(true);
//        }
//
        binding.expandedToolbar.inflateMenu(R.menu.current_point_menu);
        binding.expandedToolbar.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.action_done:
                    closeAndSave();
                    break;
            }
            return false;
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_constructor_points, container, false);
        behavior = PointsBottomSheetBehavior.from(binding.bottomSheet);
        behavior.setState(PointsBottomSheetBehavior.STATE_HIDDEN);
        binding.changeModeFab.setOnClickListener(pView -> {
            FragmentManager childFragmentManager = getChildFragmentManager();
            Fragment fragment = childFragmentManager.findFragmentById(R.id.container);
            if (fragment instanceof SupportMapFragment) {
                StepsListFragment listFragment = StepsListFragment.newInstance(mQuest.id);
                childFragmentManager.beginTransaction().add(R.id.container, listFragment).addToBackStack(null).commit();
            } else {
                childFragmentManager.popBackStack();
            }
        });
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupMapFragment();
    }

    private void setupMapFragment() {
        FragmentManager fm = getChildFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.maps_container);
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.container, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap pGoogleMap) {
        mMap = pGoogleMap;
        mMap.setOnMapClickListener(pLatLng -> openDetailFragment(pLatLng));
        mMap.setOnMarkerClickListener(pMarker -> {
            openBottomView(mQuestStepMap.get(pMarker.getPosition()), PointsBottomSheetBehavior.STATE_COLLAPSED, pMarker.getPosition());
            return false;
        });
        mPointsPresenter.loadPoints(mQuest);

        buildGoogleApiClient();

        mGoogleApiClient.connect();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void setSteps(List<QuestStep> pQuestSteps) {
        if (mMap != null) {
            if (mQuestStepMap == null) mQuestStepMap = new HashMap<>();
            for (QuestStep questStep : pQuestSteps) {
                MarkerOptions markerOptions = new MarkerOptions()
                        .anchor(0.0f, 1.0f)
                        .position(new LatLng(questStep.getMark().getLat(), questStep.getMark().getLng()));
                mMap.addMarker(markerOptions);
                mQuestStepMap.put(markerOptions.getPosition(), questStep);
            }
        }
    }


    private void openDetailFragment(LatLng pLatLng) {
        AskDialogFragment askDialogFragment = AskDialogFragment.newInstance("New point", "Are you sure you want to put new point here?");
        askDialogFragment.setYesNoListener(new AskDialogFragment.YesNoListener() {
            @Override
            public void yes() {
                MarkerOptions markerOptions = new MarkerOptions()
                        .anchor(0.0f, 1.0f)
                        .position(pLatLng);
                mMap.addMarker(markerOptions);
                QuestStep questStep = createStep();
                mQuestStepMap.put(markerOptions.getPosition(), questStep);
                openBottomView(questStep, PointsBottomSheetBehavior.STATE_ANCHOR_POINT, pLatLng);
            }

            @Override
            public void no() {

            }
        });
        askDialogFragment.show(getChildFragmentManager(), "ask_fragment");
    }

    private void openBottomView(@Nullable QuestStep pQuestStep, int pState, LatLng pLatLng) {
        if (pQuestStep != null) {
            behavior.setHideable(true);
            behavior.setPeekHeight(AndroidUtils.dp(80));
            behavior.setState(pState);
            binding.pointEditLayout.setQuestStep(pQuestStep, pLatLng);
        }
    }

    private void closeAndSave() {
        if (binding.pointEditLayout.isMarkNameEmpty()) {
            OkDialogFragment okDialogFragment = OkDialogFragment.newInstance("Point name is empty", "You should fill the point name");
            okDialogFragment.show(getChildFragmentManager(), "ok_dialog");
        } else {
            behavior.setState(PointsBottomSheetBehavior.STATE_COLLAPSED);
            QuestStep questStep = binding.pointEditLayout.getQuestStep();
            if (questStep.isNew()) {
                mPointsPresenter.createStep(questStep);
            } else
                mPointsPresenter.updateStep(questStep);
        }
    }


    private QuestStep createStep() {
        QuestStep questStep = new QuestStep();
        questStep.setQuest(mQuest);
        return questStep;
    }

    @Override
    public void onConnected(@Nullable Bundle pBundle) {

    }

    @Override
    public void onConnectionSuspended(int pI) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult pConnectionResult) {

    }
}
