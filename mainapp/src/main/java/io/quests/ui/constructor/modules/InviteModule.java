package io.quests.ui.constructor.modules;

import dagger.Module;
import dagger.Provides;
import io.quests.data.managers.InvitePresenter;
import io.quests.data.managers.InvitePresenterImpl;

/**
 * Created by andreyzakharov on 18.07.16.
 */
@Module
public class InviteModule {


    @Provides
    public InvitePresenter provideInvitePresenter() {
        return new InvitePresenterImpl();
    }
}
