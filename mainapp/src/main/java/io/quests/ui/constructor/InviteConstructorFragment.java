package io.quests.ui.constructor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.quests.ui.base.BaseFragment;

/**
 * Created by andreyzakharov on 18.07.16.
 */

public class InviteConstructorFragment extends BaseFragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    protected void setupComponent() {

    }

    @Override
    protected void setupToolbar() {

    }
}
