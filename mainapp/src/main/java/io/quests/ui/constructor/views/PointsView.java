package io.quests.ui.constructor.views;

import java.util.List;

import io.quests.data.models.QuestStep;

/**
 * Created by andreyzakharov on 17.06.16.
 */

public interface PointsView {

    void setSteps(List<QuestStep> pMarkList);
}
