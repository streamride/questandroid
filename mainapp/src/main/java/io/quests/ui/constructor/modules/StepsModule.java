package io.quests.ui.constructor.modules;

import dagger.Module;
import dagger.Provides;
import io.quests.QuestApp;
import io.quests.data.managers.StepsPresenter;
import io.quests.data.managers.StepsPresenterImpl;
import io.quests.ui.constructor.views.StepsView;

/**
 * Created by andreyzakharov on 17.06.16.
 */

@Module
public class StepsModule {


    private final StepsView mStepsView;

    public StepsModule(StepsView pStepsView) {
        mStepsView = pStepsView;
    }


    @Provides
    public StepsPresenter provideStepsPresenter(QuestApp pQuestApp) {
        return new StepsPresenterImpl(mStepsView, pQuestApp);
    }

}
