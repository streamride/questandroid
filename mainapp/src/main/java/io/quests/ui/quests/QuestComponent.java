package io.quests.ui.quests;

import dagger.Component;
import io.quests.AppComponent;
import io.quests.ui.FragmentScope;

/**
 * Created by andreyzakharov on 11.06.16.
 */
@FragmentScope
@Component(dependencies = AppComponent.class,
        modules = {QuestModule.class})
public interface QuestComponent {
    void inject(QuestFragment pQuestFragment);
}
