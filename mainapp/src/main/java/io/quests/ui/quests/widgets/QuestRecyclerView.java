package io.quests.ui.quests.widgets;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import java.util.List;

import io.quests.data.models.Quest;
import io.quests.ui.constructor.QuestActivity;

/**
 * Created by andreyzakharov on 11.06.16.
 */

public class QuestRecyclerView extends RecyclerView {

    private LinearLayoutManager mLayoutManager;
    private QuestListAdapter mQuestAdapter;

    public QuestRecyclerView(Context context) {
        super(context);
        init(context);
    }

    public QuestRecyclerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public QuestRecyclerView(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    private void init(Context pContext) {
        mLayoutManager = new LinearLayoutManager(pContext, LinearLayoutManager.VERTICAL, false);
        mQuestAdapter = new QuestListAdapter(pContext, new QuestClickListener() {
            @Override
            public void onClicked(Quest pQuest) {
                QuestActivity.startActivity(getContext(), pQuest.id, false);

            }

            @Override
            public void onLongClicked(Quest pQuest) {

            }
        });
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        setLayoutManager(mLayoutManager);
        setAdapter(mQuestAdapter);
    }

    public void replaceWith(List<Quest> pQuestList) {
        mQuestAdapter.replaceWith(pQuestList);
    }

    public interface QuestClickListener {
        void onClicked(Quest pQuest);

        void onLongClicked(Quest pQuest);
    }
}
