package io.quests.ui.quests;

import dagger.Module;
import dagger.Provides;
import io.quests.QuestApp;
import io.quests.data.managers.QuestPresenter;
import io.quests.data.managers.QuestPresenterImpl;
import io.quests.ui.FragmentScope;
import io.quests.ui.quests.views.QuestView;

/**
 * Created by andreyzakharov on 11.06.16.
 */
@Module
public class QuestModule {

    private final QuestView mQuestView;

    public QuestModule(QuestView pQuestView) {
        mQuestView = pQuestView;
    }

    @FragmentScope
    @Provides
    public QuestPresenter provideQuestPresenter(QuestApp pQuestApp) {
        return new QuestPresenterImpl(mQuestView, pQuestApp);
    }

}
