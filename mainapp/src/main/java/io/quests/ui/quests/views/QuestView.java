package io.quests.ui.quests.views;

import io.quests.ui.quests.widgets.QuestRecyclerView;

/**
 * Created by andreyzakharov on 11.06.16.
 */

public interface QuestView {

    public void showLoading();

    public void hideLoading();

    public QuestRecyclerView getQuestRecyclerView();
}
