package io.quests.ui.quests;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import io.quests.AppComponent;
import io.quests.QuestApp;
import io.quests.R;
import io.quests.data.managers.QuestPresenter;
import io.quests.ui.base.BaseFragment;
import io.quests.ui.constructor.QuestActivity;
import io.quests.ui.quests.views.QuestView;
import io.quests.ui.quests.widgets.QuestRecyclerView;

/**
 * Created by andreyzakharov on 11.06.16.
 */

public class QuestFragment extends BaseFragment implements QuestView {

    QuestComponent component;

    @Inject
    QuestPresenter mQuestPresenter;
    private QuestRecyclerView mQuestRecyclerView;
    private View view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.quest_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Intent intent = new Intent(getContext(), QuestActivity.class);
                startActivity(intent);
                break;
            case R.id.action_show_my:
                mQuestPresenter.getMyQuests(0, 0);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_quests, container, false);

        mQuestRecyclerView = (QuestRecyclerView) view.findViewById(R.id.quest_recyclerview);
        return view;
    }

    @Override
    protected void setupComponent() {
        AppComponent appComponent = (AppComponent) QuestApp.get(getActivity()).component();
        component = DaggerQuestComponent.builder()
                                        .appComponent(appComponent)
                                        .questModule(new QuestModule(this))
                                        .build();
        component.inject(this);
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mQuestPresenter.getQuest(0, 0);
    }

    @Override
    protected void setupToolbar() {
        Toolbar mToolBar = (Toolbar) view.findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(mToolBar);
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
        }
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public QuestRecyclerView getQuestRecyclerView() {
        return mQuestRecyclerView;
    }
}
