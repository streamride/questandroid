package io.quests.ui.quests.widgets;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.quests.R;
import io.quests.data.models.Quest;

/**
 * Created by andreyzakharov on 11.06.16.
 */

public class QuestListAdapter extends RecyclerView.Adapter<QuestListAdapter.QuestViewHolder> {

    private final Context mContext;
    private final LayoutInflater mLayoutInflater;
    private final QuestRecyclerView.QuestClickListener mQuestClickListener;

    private List<Quest> items = Collections.emptyList();

    public QuestListAdapter(Context pContext, QuestRecyclerView.QuestClickListener pQuestClickListener) {
        mContext = pContext;
        mLayoutInflater = LayoutInflater.from(pContext);
        mQuestClickListener = pQuestClickListener;
    }

    public void replaceWith(List<Quest> pQuestList) {
        items = new ArrayList<>(pQuestList);
        notifyDataSetChanged();
    }


    @Override
    public QuestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.item_quest, parent, false);
        QuestViewHolder questViewHolder = new QuestViewHolder(view);
        return questViewHolder;
    }

    @Override
    public void onBindViewHolder(QuestViewHolder holder, int position) {
        Quest quest = items.get(position);
        holder.mQuestNameTv.setText(quest.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View pView) {
                mQuestClickListener.onClicked(quest);
            }
        });

        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View pView) {
                mQuestClickListener.onLongClicked(quest);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    static class QuestViewHolder extends RecyclerView.ViewHolder {

        private final TextView mQuestNameTv;

        public QuestViewHolder(View itemView) {
            super(itemView);
            mQuestNameTv = (TextView) itemView.findViewById(R.id.quest_name_tv);
        }
    }
}
