package io.quests.ui.quests;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import io.quests.QuestApp;
import io.quests.R;
import io.quests.data.models.Quest;
import io.quests.databinding.FragmentQuestBinding;
import io.quests.ui.base.BaseFragment;
import io.quests.ui.constructor.QuestActivity;
import io.quests.ui.game.GameActivity;

/**
 * Created by andreyzakharov on 26.07.16.
 */
public class CurrentQuestFragment extends BaseFragment {

    private static final java.lang.String PARAM_QUEST = "param_quest";
    FragmentQuestBinding binding;
    private Quest mQuest;

    public static CurrentQuestFragment newInstance(String pQuestID) {
        CurrentQuestFragment questFragment = new CurrentQuestFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_QUEST, pQuestID);
        questFragment.setArguments(bundle);
        return questFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        String id = getArguments().getString(PARAM_QUEST);
        mQuest = QuestApp.get(getContext()).getCacheManager().getQuestById(id);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.current_quest_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_edit:
                QuestActivity.startActivity(getContext(), mQuest.id);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_quest, container, false);
        binding.startQuest.setOnClickListener(pView -> GameActivity.start(getContext(), mQuest));
        return binding.getRoot();
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        binding.questNameTv.setText(mQuest.getName());
        binding.questTypeTv.setText(mQuest.getType());
        binding.questDescTv.setText(mQuest.getAbout());
    }

    @Override
    protected void setupComponent() {

    }

    @Override
    protected void setupToolbar() {
        Toolbar mToolBar = (Toolbar) binding.getRoot().findViewById(R.id.toolbar);
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        activity.setSupportActionBar(mToolBar);
        ActionBar actionBar = activity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
        }
    }
}
