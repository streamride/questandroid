package io.quests.ui.base;

/**
 * Created by andreyzakharov on 17.06.16.
 */

public interface BaseView {

    void showLoading();

    void hideLoading();
}
