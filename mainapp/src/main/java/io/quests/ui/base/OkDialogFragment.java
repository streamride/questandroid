package io.quests.ui.base;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialogFragment;

/**
 * Created by andreyzakharov on 05.12.15.
 */
public class OkDialogFragment extends AppCompatDialogFragment {

    private static final String PARAM_TITLE = "param_title";
    private static final String PARAM_TEXT = "param_text";
    private String mTitle;
    private String mMessage;

    public static OkDialogFragment newInstance(String title, String text) {
        OkDialogFragment fragment = new OkDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_TITLE, title);
        bundle.putString(PARAM_TEXT, text);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();
        mTitle = arguments.getString(PARAM_TITLE);
        mMessage = arguments.getString(PARAM_TEXT);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setTitle(mTitle)
                .setMessage(mMessage)
                .setNeutralButton("OK", (dialog, which) -> dismiss())
                .show();

        return alertDialog;
    }

}
