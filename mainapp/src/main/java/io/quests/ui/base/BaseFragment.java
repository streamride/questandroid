package io.quests.ui.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by andreyzakharov on 15.05.16.
 */
public abstract class BaseFragment extends Fragment {

    @Override public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //all common tasks
        setupComponent();
        setupToolbar();
    }

    protected abstract void setupComponent();

    protected abstract void setupToolbar();



}
