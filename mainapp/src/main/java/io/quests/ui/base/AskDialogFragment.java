package io.quests.ui.base;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

import io.quests.R;


/**
 * Created by andreyzakharov on 15.01.16.
 */
public class AskDialogFragment extends AppCompatDialogFragment {


    private static final String PARAM_TITLE = "param_title";
    private static final String PARAM_MESSAGE = "param_message";
    private String mTitle;
    private String mMessage;
    private YesNoListener mYesNoLister;
    private String mYes;
    private String mNo;


    public static AskDialogFragment newInstance(String title, String message) {
        AskDialogFragment askDialogFragment = new AskDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(PARAM_TITLE, title);
        bundle.putString(PARAM_MESSAGE, message);
        askDialogFragment.setArguments(bundle);
        return askDialogFragment;
    }

    public void setYesNoTitle(String yes, String no) {
        mYes = yes;
        mNo = no;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey(PARAM_MESSAGE) && arguments.containsKey(PARAM_MESSAGE)) {
            mTitle = arguments.getString(PARAM_TITLE);
            mMessage = arguments.getString(PARAM_MESSAGE);
        } else {
            throw new IllegalStateException("You need to call newInstance(String title, String message) method");
        }
        mYes = getResources().getString(R.string.yes);
        mNo = getResources().getString(R.string.no);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext(), R.style.MyDialogTheme);
        builder.setTitle(mTitle)
               .setPositiveButton(mYes, (dialog, which) -> {
                   if (mYesNoLister != null) {
                       mYesNoLister.yes();
                   }
                   dismiss();

               }).setNegativeButton(mNo, (dialog, which) -> {
            if (mYesNoLister != null) {
                mYesNoLister.no();
            }
            dismiss();
        }).setMessage(mMessage);

        return builder.create();
    }

    public void setYesNoListener(YesNoListener pYesNoListener) {
        mYesNoLister = pYesNoListener;
    }


    public interface YesNoListener {
        void yes();

        void no();
    }
}
