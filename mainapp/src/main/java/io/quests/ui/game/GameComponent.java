package io.quests.ui.game;

/**
 * Created by andreyzakharov on 11.06.16.
 */

import dagger.Component;
import io.quests.AppComponent;
import io.quests.ui.ActivityScope;

@ActivityScope
@Component(dependencies = AppComponent.class,
        modules = GameModule.class)
public interface GameComponent {
    void inject(GameFragment pGameFragment);

}
