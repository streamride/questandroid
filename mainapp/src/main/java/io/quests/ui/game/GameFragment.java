package io.quests.ui.game;

import android.Manifest;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import javax.inject.Inject;

import io.quests.AppComponent;
import io.quests.QuestApp;
import io.quests.R;
import io.quests.data.managers.game.GamePresenter;
import io.quests.data.models.Mark;
import io.quests.data.models.Quest;
import io.quests.data.models.QuestStep;
import io.quests.databinding.FragmentGameBinding;
import io.quests.ui.base.BaseFragment;
import io.quests.ui.game.views.GameView;

/**
 * Created by andreyzakharov on 23.07.16.
 */

public class GameFragment extends BaseFragment implements GameView,
                                                          OnMapReadyCallback,
                                                          GoogleApiClient.ConnectionCallbacks,
                                                          GoogleApiClient.OnConnectionFailedListener,
                                                          LocationListener {

    private static final int PERMISSION_CODE = 12;
    private static final int ANIMATE_SPEEED_TURN = 1000;
    FragmentGameBinding binding;
    private Quest mQuest;
    private SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    GameComponent component;
    @Inject
    GamePresenter mGamePresenter;


    public static GameFragment newInstance(String pParam_quest) {
        GameFragment gameFragment = new GameFragment();
        Bundle bundle = new Bundle();
        bundle.putString("param_quest", pParam_quest);
        gameFragment.setArguments(bundle);
        return gameFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String questId = getArguments().getString("param_quest");
        mQuest = QuestApp.get(getContext()).getCacheManager().getQuestById(questId);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_game, container, false);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setupMapFragment();
    }

    private void setupMapFragment() {
        FragmentManager fm = getChildFragmentManager();
        mapFragment = (SupportMapFragment) fm.findFragmentById(R.id.maps_container);
        if (mapFragment == null) {
            mapFragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.maps_container, mapFragment).commit();
        }
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void setupComponent() {
        AppComponent appComponent = (AppComponent) QuestApp.get(getActivity()).component();
        component = DaggerGameComponent.builder()
                                       .appComponent(appComponent)
                                       .gameModule(new GameModule(this))
                                       .build();
        component.inject(this);
    }

    @Override
    protected void setupToolbar() {
//        toolbar.setPadding(0, getStatusBarHeight(), 0, 0);
    }


    @Override
    public void onConnected(@Nullable Bundle pBundle) {
        createLocationRequest();
        startLocationUpdates();

    }

    @Override
    public void onConnectionSuspended(int pI) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult pConnectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap pGoogleMap) {
        mMap = pGoogleMap;


        buildGoogleApiClient();

        mGoogleApiClient.connect();

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            setMyLocation();
        } else {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_CODE);
        }
        loadQuestSteps();

    }

    private void setMyLocation() {
        mMap.setMyLocationEnabled(true);
    }

    private void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    private void loadQuestSteps() {
        mGamePresenter.loadSteps(mQuest);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onLocationChanged(Location pLocation) {

    }

    @Override
    public void setSteps(List<QuestStep> pQuestStepList) {
        if (mMap != null) {
//            if (mQuestStepMap == null) mQuestStepMap = new HashMap<>();
            for (QuestStep questStep : pQuestStepList) {
                MarkerOptions markerOptions = new MarkerOptions()
                        .anchor(0.0f, 1.0f)
                        .position(new LatLng(questStep.getMark().getLat(), questStep.getMark().getLng()));
                mMap.addMarker(markerOptions);
//                mQuestStepMap.put(markerOptions.getPosition(), questStep);
            }
        }
        Mark mark = pQuestStepList.get(0).getMark();
        showFirstMarket(new LatLng(mark.getLat(), mark.getLng()));
    }

    private void showFirstMarket(LatLng pLatLng) {
        CameraPosition cameraPosition =
                new CameraPosition.Builder()
                        .target(pLatLng)
                        .bearing(45)
                        .tilt(90)
                        .zoom(18)
                        .build();

        new Handler().postDelayed(() -> mMap.animateCamera(
                CameraUpdateFactory.newCameraPosition(cameraPosition),
                ANIMATE_SPEEED_TURN,
                new GoogleMap.CancelableCallback() {

                    @Override
                    public void onFinish() {
                    }

                    @Override
                    public void onCancel() {
                    }
                }
                                                  ), 100);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_CODE:
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    setMyLocation();
                }
                break;
        }
    }


}
