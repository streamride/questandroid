package io.quests.ui.game.views;

import java.util.List;

import io.quests.data.models.QuestStep;

/**
 * Created by andreyzakharov on 26.07.16.
 */

public interface GameView {

    void setSteps(List<QuestStep> pQuestStepList);
}
