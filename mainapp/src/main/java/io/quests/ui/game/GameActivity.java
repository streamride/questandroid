package io.quests.ui.game;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import io.quests.R;
import io.quests.data.models.Quest;
import io.quests.ui.base.BaseActivity;

/**
 * Created by andreyzakharov on 11.06.16.
 */

public class GameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragmentcontainer);

        GameFragment fragment = GameFragment.newInstance(getIntent().getStringExtra("param_quest"));
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, fragment).commitNowAllowingStateLoss();
    }

    public static void start(Context pContext, Quest pQuest) {
        Intent intent = new Intent(pContext, GameActivity.class);
        intent.putExtra("param_quest", pQuest.id);
        pContext.startActivity(intent);
    }
}
