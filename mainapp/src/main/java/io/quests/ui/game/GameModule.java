package io.quests.ui.game;

import dagger.Module;
import dagger.Provides;
import io.quests.QuestApp;
import io.quests.data.managers.game.GamePresenter;
import io.quests.data.managers.game.GamePresenterImpl;
import io.quests.ui.game.views.GameView;

/**
 * Created by andreyzakharov on 26.07.16.
 */
@Module
public class GameModule {

    private final GameView mGameView;

    public GameModule(GameView pGameView) {
        mGameView = pGameView;
    }

    @Provides
    public GamePresenter providesPointsPresenter(QuestApp pQuestApp) {
        return new GamePresenterImpl(mGameView, pQuestApp);
    }
}
