package io.quests.utils;

import android.content.Context;
import android.content.SharedPreferences;

import io.quests.QuestApp;

/**
 * Created by andreyzakharov on 11.06.16.
 */

public class PreferencesManager {

    private static final String SETTINGS_PACKAGE_NAME = "io.quests.settings";
    private final QuestApp mApp;

    public PreferencesManager(QuestApp pQuestApp) {
        mApp = pQuestApp;
    }

    private SharedPreferences getSharedPreference() {
        return mApp.getApplicationContext().getSharedPreferences(SETTINGS_PACKAGE_NAME, Context.MODE_PRIVATE);
    }

    public String getCurrentUserId() {
        return "";
    }

    public void setAuthToken(String pAccessToken) {
        getSharedPreference().edit().putString("token", pAccessToken).apply();
    }

    public String getAuthToken() {
        return getSharedPreference().getString("token", "");
    }

}
