package io.quests.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Build;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import java.lang.reflect.Method;

import io.quests.QuestApp;

/**
 * Created by andreyzakharov on 27.08.15.
 */
public class AndroidUtils {


    public static float density = 1;

    static {
        density = QuestApp.applicationContext.getResources().getDisplayMetrics().density;
    }

    public static int getCacheSize() {
        int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        int cacheSize = maxMemory / 8;
        return cacheSize;
    }

    public static int dp(float value) {
        if (value == 0)
            return 0;

        return (int) Math.ceil(density * value);
    }

    public static int getNavigationBarSize() {
        Resources resources = QuestApp.applicationContext.getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    public int getStatusBarHeight() {
        Resources resources = QuestApp.applicationContext.getResources();
        int result = 0;
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public static Point getRealScreenSize() {
        Point point = new Point();
        try {
            WindowManager windowManager = (WindowManager) QuestApp.applicationContext.getSystemService(Context.WINDOW_SERVICE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                windowManager.getDefaultDisplay().getRealSize(point);
            } else {
                try {
                    Method getRawWidth = Display.class.getMethod("getRawWidth");
                    Method getRawHeight = Display.class.getMethod("getRawHeight");
                    point.set(((Integer) getRawWidth.invoke(windowManager.getDefaultDisplay())),
                              ((Integer) getRawHeight.invoke(windowManager.getDefaultDisplay())));
                } catch (Exception e) {
                    //
                }
            }
        } catch (Exception e) {
            //
        }

        return point;
    }

    public static int getMaxWidthWithPadding() {
        return getRealScreenSize().x - AndroidUtils.dp(32);
    }

    public static boolean isBiggerThanLollipop() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    public static boolean isBiggerThanMarshmallow() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M;
    }


    public static int getMaxWidth() {
        return getRealScreenSize().x;
    }

    public static int getMaxHeight() {
        return getRealScreenSize().y;
    }


    public static void showKeyboard(View pView) {
        if (pView == null)
            return;


        InputMethodManager inputMethodManager = (InputMethodManager) pView.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        pView.requestFocus();
        inputMethodManager.showSoftInput(pView, 0);
//        inputMethodManager.showSoftInput(pView, InputMethodManager.SHOW_IMPLICIT);
    }


    public static void hideKeyboard(View view) {
        if (view == null)
            return;
        InputMethodManager inputMethodManager = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (!inputMethodManager.isActive())
            return;
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


}
