package io.quests.db;

import java.util.List;

import io.quests.data.models.Mark;
import io.quests.data.models.Quest;
import io.quests.data.models.QuestStep;
import io.quests.data.models.User;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by andreyzakharov on 15.05.16.
 */
public class CacheManager implements ICacheManager {

    private final DBManager dbManager;

    public CacheManager(DBManager pDBManager) {
        dbManager = pDBManager;

    }

    @Override
    public void saveUser(User pUser) {
        Realm realm = DBManager.getInstance();
        realm.executeTransactionAsync(realm1 -> {
            User user = realm1.copyToRealm(pUser);
        });
    }

    @Override
    public RealmResults<Quest> getMyQuests() {
        Realm realm = DBManager.getInstance();
        RealmResults<Quest> quests = realm.where(Quest.class).equalTo("creator.id", dbManager.getQuestApp().getSharedPreferences().getCurrentUserId()).findAll();
        return quests;
    }

    public void saveQuest(Quest pQuest) {
        try {
            Realm realm = DBManager.getInstance();
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(pQuest);
            realm.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void saveMark(Mark pMark) {
        Realm realm = DBManager.getInstance();
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(pMark);
        realm.commitTransaction();
    }


    public User getCurrentUser() {
        Realm realm = DBManager.getInstance();
        String userId = dbManager.getQuestApp().getSharedPreferences().getCurrentUserId();
        User user = realm.where(User.class).equalTo("id", userId).findFirst();
        return user;
    }

    public RealmResults<Quest> getActiveQuests() {
        Realm realm = DBManager.getInstance();
        RealmResults<Quest> quests = realm.where(Quest.class).equalTo("isDraft", false).findAll();
        return quests;
    }

    public void saveQuests(List<Quest> pQuestList) {
        Realm realm = DBManager.getInstance();
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(pQuestList);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            realm.commitTransaction();
        }

    }

    public Quest getQuestById(String pQuestId) {
        Realm realm = DBManager.getInstance();
        Quest quest = realm.where(Quest.class).equalTo("id", pQuestId).findFirst();
        return quest;
    }

    public List<QuestStep> getSteps(Quest pQuest) {
        Realm realm = DBManager.getInstance();
        RealmResults<QuestStep> stepsList = realm.where(QuestStep.class).equalTo("quest.id", pQuest.id).findAll();
        return stepsList;
    }

    public void saveSteps(List<QuestStep> pQuestSteps) {
        Realm realm = DBManager.getInstance();
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(pQuestSteps);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            realm.commitTransaction();
        }
    }

    public void saveStep(QuestStep pQuestStep) {
        Realm realm = DBManager.getInstance();
        try {
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(pQuestStep);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            realm.commitTransaction();
        }
    }
}
