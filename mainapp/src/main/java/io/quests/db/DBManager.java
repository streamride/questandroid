package io.quests.db;

import android.content.Context;

import java.security.SecureRandom;

import javax.inject.Inject;

import io.quests.QuestApp;
import io.quests.data.models.Mark;
import io.quests.data.models.MarkGroup;
import io.quests.data.models.Quest;
import io.quests.data.models.QuestStep;
import io.quests.data.models.User;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.annotations.RealmModule;

/**
 * Created by andreyzakharov on 02.06.16.
 */
@RealmModule(classes = {User.class, Quest.class, Mark.class, MarkGroup.class, QuestStep.class})
public class DBManager {

    static final String DB_NAME = "quests";
    private final QuestApp mQuestApp;

    private RealmConfiguration configuration;
    private Context context;

    @Inject
    public DBManager(QuestApp pQuestApp) {
        mQuestApp = pQuestApp;
        this.context = pQuestApp.getApplicationContext();
        byte[] key = new byte[64];
        new SecureRandom().nextBytes(key);
        this.configuration = new RealmConfiguration.Builder(context)
                .deleteRealmIfMigrationNeeded()
//                .name(DB_NAME)
//                .encryptionKey(key)
                .modules(this)
                .build();
        Realm.setDefaultConfiguration(configuration);
    }

    public static Realm getInstance() {
        return Realm.getDefaultInstance();
    }

    public QuestApp getQuestApp() {
        return mQuestApp;
    }
}
