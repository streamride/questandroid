package io.quests.db;

import io.quests.data.models.Quest;
import io.quests.data.models.User;
import io.realm.RealmResults;

/**
 * Created by andreyzakharov on 15.05.16.
 */
public interface ICacheManager {

    void saveUser(User pUser);

    RealmResults<Quest> getMyQuests();
}
